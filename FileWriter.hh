<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/

namespace nuclio\plugin\fileSystem\writer
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\plugin\fileSystem\writer\FileWriterException;
	use nuclio\plugin\provider\manager\Manager as ProviderManager;
	use nuclio\plugin\fileSystem\driver\common\CommonInterface;
	
	/**
	 * File management.
	 *
	 * This class provides CRUD functionlities on a file.
	 * 
	 */
	<<factory>>
	class FileWriter extends Plugin
	{
		private CommonInterface $driver; 
		private string 			$file;
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):FileWriter
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		
		public function __construct(string $driver, string $path)
		{
			parent::__construct();
			
			$tmpDriver=ProviderManager::request($driver);
			if($tmpDriver instanceof CommonInterface)
			{
				$this->driver = $tmpDriver;
			}
			else
			{
				throw new FileWriterException(sprintf('"%s" is not a valid driver.',$tmpDriver));
			}
			if(!$this->driver->exists($path))
			{
				//make it
				$this->driver->touch($path);
			}
			$this->file = $path;
		}
		
		/**
		 * Get the used driver name
		 * 
		 * @access private
		 *
		 * @return     CommonInterface  Driver Name.
		 */
		private function getDriver():CommonInterface
		{
			return $this->driver;
		}
		
		/**
		 * Check if the given path is writeable.
		 * 
		 * @access public
		 * 
		 * @param  string  $path Path of directory
		 * @return boolean       True/False for writable or not.
		 */
		public function isWritable():bool
		{
			return $this->getDriver()->isWritable($this->file);
		}
		
		/**
		 * Write content to a file.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return mixed       	Return file content if exist and error if file does not exist.
		 */ 
		public function write(string $contents):bool
		{
			if(!$this->isWritable())
			{
				throw new FileWriterException(sprintf('"%s" is not writable.'),$this->file);
			}
			return $this->getDriver()->write($this->file, $contents);
		}
		
		/**
		 * Write content to a file.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return mixed       	Return file content if exist and error if file does not exist.
		 */
		public function append(string $contents):mixed
		{
			if(!$this->isWritable())
			{
				throw new FileWriterException("Not writeable!");
			}
			return $this->getDriver()->append($this->file, $contents);
		}
		
		/**
		 * Deletes a file
		 * 
		 * @access public
		 * 
		 * @param  int    $rules unused
		 * @return mixed  true on success, error message on failure
		 */
		public function delete(int $rules):bool
		{
			return $this->getDriver()->delete($this->file, $rules);
		}
		
		/**
		 * Get the file name.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return string      	File name
		 */
		public function getName():?string
		{
			return $this->getDriver()->getName($this->file);
		}
		
		/**
		 * Get the file extension.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return string      	File extension
		 */
		public function getExt():?string
		{
			return $this->getDriver()->getExt($this->file);
		}
		
		/**
		 * Get the file type.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return string      	File type
		 */
		public function getType():?string
		{
			return $this->getDriver()->getType($this->file);
		}
		
		/**
		 * Get the file size.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return int      	File size
		 */
		public function getSize():?int
		{
			return $this->getDriver()->getSize($this->file);
		}
	}
}
